Task
===========

- 3 контейнера nginx1 nginx2 grafana (https://hub.docker.com/r/grafana/grafana)
- Nginx1 port 8001 на Grafana
- Nginx2 port 8002 на Grafana
- Grafana не пробрасывать ничего наружу. Она не должна подниматься если вы её стопорите руками.
- Nginx1 и Nginx2 в разных сетях.
- 2 конфига нжинкса которые будут пробрасывать на Grafana
- Сделать хелсчек Nginx1 в контейнере с Grafana
- Уронить Nginx1 посмотреть что будет

Description
===========

- 8001.conf - config file for nginx1
- 8002.conf - config file for nginx2
- 3 networks, backend - for grafana, frontend1 - for nginx1, frontend2 - for nginx2
- healthcheck for all containers, but healthcheck inside contaener grafana can check nginx1
- containers nginx1, nginx2 acts as a proxy for grafana

![healthcheck](https://gitlab.com/nils7vk/upway/-/raw/main/less1.6/healthcheck.jpg "healthcheck")
![grafana](https://gitlab.com/nils7vk/upway/-/raw/main/less1.6/Grafana.jpg "grafana")