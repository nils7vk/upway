Task
===========

- Иногда контейнерам нужен общий волюм в режиме ридонли.
- Что бы закрепить тему с волюмами сделаем задачу.
- Берем композ с прошлого задания (2 нжинкса + графана)
- Модифицируем его таким образом, что бы не копировать конфиги каждому контейнеру, а создали в композе волюм и подключили бы его к 2м нжинксам.
- Ссылок не будет, просмотрев видео мы уже должны иметь представления что такое волюмы, но там не было этого простого момента.
- Подсказка: local driver

Description
===========

One volume with 2 files 8001.conf and 8002.conf
- 8001.conf - config file for nginx1
- 8002.conf - config file for nginx2
- 3 networks, backend - for grafana, frontend1 - for nginx1, frontend2 - for nginx2
- healthcheck for all containers, but healthcheck inside contaener grafana can check nginx1
- containers nginx1, nginx2 acts as a proxy for grafana

![rmconf](https://gitlab.com/nils7vk/upway/-/raw/main/less1.8/rmconf.jpg "rmconf")
